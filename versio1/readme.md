.ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    font-family: Arial, Helvetica, sans-serif;
    overflow: hidden;
    background-color: #B87333;
}

li {
    float: left;
}

li a, .dropbtn {
    display: inline-block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

li a:hover, .dropdown:hover .dropbtn {
    background-color: rgb(141, 86, 34);
}
li.dropdown {
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color:  #B87333;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
    color: white;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
    text-align: left;
}

.dropdown-content a:hover {background-color:rgb(141, 86, 34)}

.dropdown:hover .dropdown-content {
    display: block;
}
.cine{
    font-family: Arial, Helvetica, sans-serif;
    text-align: left;
    float: none;
}

@font-face{
    font-family:fontP;
    src: url(fonts/god.ttf);
}

@font-face{
    font-family:final;
    src: url(fonts/Cybrpnuk2.ttf);
}

@font-face{
    font-family:subt;
    src: url(fonts/RoyaliteScript.ttf);
}

h1{
    font-family:fontP;

}
h2{
    font-family: final;
}
h3{
    font-family: subt;
    text-decoration: underline black;
    font-size: 25px;
}

p{
    font-family: Arial, Helvetica, sans-serif;
}
.lletra{
    font-family: Arial, Helvetica, sans-serif;
}

body{
    background-color: rgba(247, 247, 140, 0.568);
}
.wrapper{
    display: flex;
    flex-direction: row;
    height: 700px;
}

.gallery{
    order: 1;
    display: flex;
    flex-direction: column;
    flex-grow: 1;
}

.gal-item{
    padding: 20px;
}
.gal-item:hover{
    background-color: rgba(182, 182, 104, 0.568)
}
.video{
    order: 2;
    display: flex;
    flex-direction: column;
    flex-grow: 1;
}